#include <stdio.h>
#include <string.h>
#include <signal.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
#include <iostream>

#include <sys/stat.h>

int main() {
	
	mkfifo("/tmp/FIFOprac2",0777); //crear tuberia con nombre; 0777 = permisos de lectura, escritura y ejecución para todo el mundo).	

	int pipe = open("/tmp/FIFOprac2",O_RDONLY);
	char cad[1000]; 

	for(;;)
	{
 		read(pipe,cad,sizeof(cad)); 

		std::cout << cad << std::endl;
	}

	 close(pipe);
	 unlink("/tmp/FIFOprac2");

	 return 0;
} 
