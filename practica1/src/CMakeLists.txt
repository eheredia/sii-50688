INCLUDE_DIRECTORIES("${PROJECT_INCLUDE_DIR}")


find_package (Threads)

SET(CLIENTE_SRCS
	MundoCliente.cpp
	Esfera.cpp
	Plano.cpp
	Raqueta.cpp
	Vector2D.cpp
	Socket.cpp)
SET(SERVIDOR_SRCS
	MundoServidor.cpp
	Esfera.cpp
	Plano.cpp
	Raqueta.cpp
	Vector2D.cpp
	Socket.cpp)

SET(COMMON_SRCS_logger
	logger.cpp)
				
ADD_EXECUTABLE(servidor servidor.cpp ${SERVIDOR_SRCS})
ADD_EXECUTABLE(cliente cliente.cpp ${CLIENTE_SRCS})
ADD_EXECUTABLE(logger logger.cpp)
ADD_EXECUTABLE(bot bot.cpp)

TARGET_LINK_LIBRARIES(cliente glut GL GLU)
TARGET_LINK_LIBRARIES(servidor glut GL GLU)
target_link_libraries (servidor ${CMAKE_THREAD_LIBS_INIT})
