// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <error.h>
#include <iostream>

#include <unistd.h>
#include <fcntl.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoCliente::CMundoCliente()
{
	Init();
}

CMundoCliente::~CMundoCliente()
{
	//cerrar la tuberia correctamente
	//close (pipe2);
 	//unlink ("/tmp/FIFOprac3");
	close (pipe3);
	unlink ("/tmp/FIFOprac3_1");
}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO//Socket	
	srvr.Connect("192.168.124.139", 2100);

	printf("conected\n");

	printf("write a name:\n");

	setbuf(stdin,NULL);
	scanf("%s",status);

	printf("sending\n");

	srvr.Send(status, 200*sizeof(char));

	printf("sent\n");

	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////FIFOprac3

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{	
/*
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
	}


	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;
		
	}

	//Para el bot, actualizo los datos de datosMemCompartida
	pdatos->esfera = esfera;
	pdatos->raqueta1 = jugador1;

	//1 arriba, 0 nada, -1 abajo
	if (pdatos->accion == 1)  
		OnKeyboardDown('w',0,0);
	if (pdatos->accion == -1)  
		OnKeyboardDown('s',0,0);

	//salida del juego por puntos
	if(puntos1==4 || puntos2==4)
		exit(0);
*/
	
	/*
	char cadPrac3[CADENA];
	read(pipe2, cadPrac3,CADENA*sizeof(char));
	sscanf(cadPrac3,"%f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2);
	*/

	printf("reciving\n");
	servidor.Receive(estado, 200*sizeof(char));
	printf("recived\n");

	sscanf(estado,"%f %f %f %f %f %f %f %f %f %f %f %f %d %d %f", &esfera.centro.x,&esfera.centro.y, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2,&jugador1.centro,&jugador2.centro, &puntos1, &puntos2, &esfera.radio); 

	if(puntos1 == 3 || puntos2 == 3){
			printf("fin\n");
			exit(1);
	}

	pdatos->raqueta1 = jugador2;
	pdatos->esfera = esfera;
	
	printf("bot_start\n");
	if(pdatos->accion < 0) teclaJugador[1] = 'l';
	else if(pdatos->accion > 0)  teclaJugador[1] = 'o';
	else printf("error\n");;
	printf("bot_end\n");

	servidor.Send(teclaJugador, 3*sizeof(char));
	

}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
/*
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
*/
	
	sprintf(cadenaPrac3_1,"%c", key);
	write(pipe3,cadenaPrac3_1,sizeof(cadenaPrac3_1));

	teclaJugador[0] = key;
	servidor.Send(teclaJugador, 3*sizeof(char));
	
}

void CMundoCliente::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	//mkfifo("/tmp/FIFOprac3",0777);
	mkfifo("/tmp/FIFOprac3_1",0777);

	//pipe2 = open("/tmp/FIFOprac3", O_WRONLY);
	pipe3 = open("/tmp/FIFOprac3_1", O_WRONLY);

	//Creacion del fichero donde compartiremos 
	int file=open("/tmp/datosCompartidos.txt",O_RDWR|O_CREAT|O_TRUNC, 0666);
	write(file,&datos,sizeof(datos));

	char* org=(char*)mmap(NULL,sizeof(datos),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);

	pdatos=(DatosMemCompartida*)org;
	
	pdatos->accion=0;

	//Socket	
	servidor.Connect("192.168.124.139", 2100);

	printf("conected\n");

	printf("write a name:\n");

	setbuf(stdin,NULL);
	scanf("%s",status);

	printf("sending\n");

	servidor.Send(status, 200*sizeof(char));

	printf("sent\n");

}

void CMundoCliente::RecibeComandosJugador()
{
     while (1) {
            usleep(10);
            char cad[CADENA];
            read(pipe3, cad, sizeof(cad));
            unsigned char key;
            sscanf(cad,"%c",&key);
            if(key=='s')jugador1.velocidad.y=-4;
            if(key=='w')jugador1.velocidad.y=4;
            if(key=='l')jugador2.velocidad.y=-4;
            if(key=='o')jugador2.velocidad.y=4;
      }
}
