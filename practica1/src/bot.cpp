#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include "DatosMemCompartida.h"

int main()
{

	DatosMemCompartida* dat;

	int fileTXT=open("/tmp/datosCompartidos.txt",O_RDWR);
	
	char* org=(char*)mmap(NULL,sizeof(*(dat)),PROT_WRITE|PROT_READ,MAP_SHARED,fileTXT,0);

	close(fileTXT);
	
	dat=(DatosMemCompartida*)org;

	while(1)
	{
		float posicion = (dat->raqueta1.y2+dat->raqueta1.y1)/2;

		if(posicion<dat->esfera.centro.y)
			dat->accion=1;

		else if(posicion>dat->esfera.centro.y)
			dat->accion=-1;

		else
			dat->accion=0;

		usleep(25000);

	}
	munmap(org,sizeof(*(dat)));
	return 0;
} 
